#!/bin/sh

export PATH="/opt/cross/x86_64-w64-mingw32/bin:$PATH"
export HOST=x86_64-w64-mingw32
export SYSTEM=MINGW64  # Used by the OpenSSL config script

exec ./build.sh
